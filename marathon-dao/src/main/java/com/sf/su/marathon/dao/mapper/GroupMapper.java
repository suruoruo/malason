package com.sf.su.marathon.dao.mapper;

import com.sf.su.marathon.dao.domain.Group;
import org.apache.ibatis.annotations.Param;
import com.sf.su.marathon.dao.dto.GroupDetailInfo;

import java.util.List;

public interface GroupMapper {
    int deleteByPrimaryKey(String id);

    int insert(Group record);

    int insertSelective(Group record);

    Group selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Group record);

    int updateByPrimaryKey(Group record);

    List<Group> selectNotFullAndExpire();

    List<Group> getByMarketId(@Param("mktId")String mktId);

    List<GroupDetailInfo> getGroupList();
}