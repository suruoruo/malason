package com.sf.su.marathon.dao.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

@ApiModel("参团人员列表")
public class JoinGroupList {
    private Integer id;

    @ApiModelProperty("集货团ID")
    private String groupId;

    @ApiModelProperty("用户ID")
    private String userId;

    @ApiModelProperty("参团时间")
    private Date joinTime;

    public JoinGroupList(Integer id, String groupId, String userId, Date joinTime) {
        this.id = id;
        this.groupId = groupId;
        this.userId = userId;
        this.joinTime = joinTime;
    }

    public JoinGroupList() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId == null ? null : groupId.trim();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public Date getJoinTime() {
        return joinTime;
    }

    public void setJoinTime(Date joinTime) {
        this.joinTime = joinTime;
    }
}