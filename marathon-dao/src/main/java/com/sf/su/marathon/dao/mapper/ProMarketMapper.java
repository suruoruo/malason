package com.sf.su.marathon.dao.mapper;

import com.sf.su.marathon.dao.domain.ProMarket;

import java.util.List;

public interface ProMarketMapper {
    int deleteByPrimaryKey(String mktId);

    int insert(ProMarket record);

    int insertSelective(ProMarket record);

    ProMarket selectByPrimaryKey(String mktId);

    int updateByPrimaryKeySelective(ProMarket record);

    int updateByPrimaryKeyWithBLOBs(ProMarket record);

    int updateByPrimaryKey(ProMarket record);

    List<ProMarket> selectAll();

    List<ProMarket> getMarketList();
}