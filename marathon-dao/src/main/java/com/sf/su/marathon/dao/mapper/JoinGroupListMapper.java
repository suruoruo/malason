package com.sf.su.marathon.dao.mapper;

import com.sf.su.marathon.dao.domain.JoinGroupList;
import com.sf.su.marathon.dao.domain.User;

import java.util.List;

public interface JoinGroupListMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(JoinGroupList record);

    int insertSelective(JoinGroupList record);

    JoinGroupList selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(JoinGroupList record);

    int updateByPrimaryKey(JoinGroupList record);

    int countUserByGroup(String groupId);

    List<User> getUserInGroup(String groupId);
}