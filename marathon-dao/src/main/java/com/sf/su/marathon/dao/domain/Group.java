package com.sf.su.marathon.dao.domain;



import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

@ApiModel("集货团表")
public class Group {


    private String id;

    @ApiModelProperty("专业市场表")
    private String proMarketId;

    @ApiModelProperty("集货团名称")
    private String groupName;

    @ApiModelProperty("所属第几期")
    private String periodIndex;

    @ApiModelProperty("到期时间")
    private Date expireDate;

    @ApiModelProperty("当前多少人已报名")
    private Integer currentCount;

    @ApiModelProperty("总共多少人已报名")
    private Integer totalCount;

    @ApiModelProperty("是否满员")
    private Boolean isFull;

    @ApiModelProperty("类型：1.全国集货  2.地区性集货")
    private Integer type;

    @ApiModelProperty("集货团状态 1.集货中  2.时间已截止  3.集货已满")
    private Integer state;

    @ApiModelProperty("到期时间")
    private Date createdTime;

    public Group(String id, String proMarketId, String groupName, String periodIndex, Date expireDate, Integer currentCount, Integer totalCount, Boolean isFull, Integer type, Integer state, Date createdTime) {
        this.id = id;
        this.proMarketId = proMarketId;
        this.groupName = groupName;
        this.periodIndex = periodIndex;
        this.expireDate = expireDate;
        this.currentCount = currentCount;
        this.totalCount = totalCount;
        this.isFull = isFull;
        this.type = type;
        this.state = state;
        this.createdTime = createdTime;
    }

    public Group() {
        super();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getProMarketId() {
        return proMarketId;
    }

    public void setProMarketId(String proMarketId) {
        this.proMarketId = proMarketId == null ? null : proMarketId.trim();
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName == null ? null : groupName.trim();
    }

    public String getPeriodIndex() {
        return periodIndex;
    }

    public void setPeriodIndex(String periodIndex) {
        this.periodIndex = periodIndex == null ? null : periodIndex.trim();
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public Integer getCurrentCount() {
        return currentCount;
    }

    public void setCurrentCount(Integer currentCount) {
        this.currentCount = currentCount;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Boolean getIsFull() {
        return isFull;
    }

    public void setIsFull(Boolean isFull) {
        this.isFull = isFull;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }
}