package com.sf.su.marathon.dao.domain;

import java.math.BigDecimal;
import java.util.Date;

public class ProMarket {
    private String mktId;

    private String mktNameShow;

    private Short dailyMinPackages;

    private BigDecimal weightMin;

    private BigDecimal weightMax;

    private BigDecimal basePrice;

    private BigDecimal baseWeight;

    private Integer groupLimit;

    private Integer groupDuration;

    private Integer currentPeriod;

    private Date lastGroupTime;

    private Integer promiseDuration;

    private Boolean isDeleted;

    private Date createdTime;

    private Date modifiedTime;

    private String imgUrl;

    private String useRequire;

    public ProMarket(String mktId, String mktNameShow, Short dailyMinPackages, BigDecimal weightMin, BigDecimal weightMax, BigDecimal basePrice, BigDecimal baseWeight, Integer groupLimit, Integer groupDuration, Integer currentPeriod, Date lastGroupTime, Integer promiseDuration, Boolean isDeleted, Date createdTime, Date modifiedTime, String imgUrl, String useRequire) {
        this.mktId = mktId;
        this.mktNameShow = mktNameShow;
        this.dailyMinPackages = dailyMinPackages;
        this.weightMin = weightMin;
        this.weightMax = weightMax;
        this.basePrice = basePrice;
        this.baseWeight = baseWeight;
        this.groupLimit = groupLimit;
        this.groupDuration = groupDuration;
        this.currentPeriod = currentPeriod;
        this.lastGroupTime = lastGroupTime;
        this.promiseDuration = promiseDuration;
        this.isDeleted = isDeleted;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
        this.imgUrl = imgUrl;
        this.useRequire = useRequire;
    }

    public ProMarket(String mktId, String mktNameShow, Short dailyMinPackages, BigDecimal weightMin, BigDecimal weightMax, BigDecimal basePrice, BigDecimal baseWeight, Integer groupLimit, Integer groupDuration, Integer currentPeriod, Date lastGroupTime, Integer promiseDuration, Boolean isDeleted, Date createdTime, Date modifiedTime, String imgUrl) {
        this.mktId = mktId;
        this.mktNameShow = mktNameShow;
        this.dailyMinPackages = dailyMinPackages;
        this.weightMin = weightMin;
        this.weightMax = weightMax;
        this.basePrice = basePrice;
        this.baseWeight = baseWeight;
        this.groupLimit = groupLimit;
        this.groupDuration = groupDuration;
        this.currentPeriod = currentPeriod;
        this.lastGroupTime = lastGroupTime;
        this.promiseDuration = promiseDuration;
        this.isDeleted = isDeleted;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
        this.imgUrl = imgUrl;
    }

    public ProMarket() {
        super();
    }

    public String getMktId() {
        return mktId;
    }

    public void setMktId(String mktId) {
        this.mktId = mktId == null ? null : mktId.trim();
    }

    public String getMktNameShow() {
        return mktNameShow;
    }

    public void setMktNameShow(String mktNameShow) {
        this.mktNameShow = mktNameShow == null ? null : mktNameShow.trim();
    }

    public Short getDailyMinPackages() {
        return dailyMinPackages;
    }

    public void setDailyMinPackages(Short dailyMinPackages) {
        this.dailyMinPackages = dailyMinPackages;
    }

    public BigDecimal getWeightMin() {
        return weightMin;
    }

    public void setWeightMin(BigDecimal weightMin) {
        this.weightMin = weightMin;
    }

    public BigDecimal getWeightMax() {
        return weightMax;
    }

    public void setWeightMax(BigDecimal weightMax) {
        this.weightMax = weightMax;
    }

    public BigDecimal getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(BigDecimal basePrice) {
        this.basePrice = basePrice;
    }

    public BigDecimal getBaseWeight() {
        return baseWeight;
    }

    public void setBaseWeight(BigDecimal baseWeight) {
        this.baseWeight = baseWeight;
    }

    public Integer getGroupLimit() {
        return groupLimit;
    }

    public void setGroupLimit(Integer groupLimit) {
        this.groupLimit = groupLimit;
    }

    public Integer getGroupDuration() {
        return groupDuration;
    }

    public void setGroupDuration(Integer groupDuration) {
        this.groupDuration = groupDuration;
    }

    public Integer getCurrentPeriod() {
        return currentPeriod;
    }

    public void setCurrentPeriod(Integer currentPeriod) {
        this.currentPeriod = currentPeriod;
    }

    public Date getLastGroupTime() {
        return lastGroupTime;
    }

    public void setLastGroupTime(Date lastGroupTime) {
        this.lastGroupTime = lastGroupTime;
    }

    public Integer getPromiseDuration() {
        return promiseDuration;
    }

    public void setPromiseDuration(Integer promiseDuration) {
        this.promiseDuration = promiseDuration;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl == null ? null : imgUrl.trim();
    }

    public String getUseRequire() {
        return useRequire;
    }

    public void setUseRequire(String useRequire) {
        this.useRequire = useRequire == null ? null : useRequire.trim();
    }
}