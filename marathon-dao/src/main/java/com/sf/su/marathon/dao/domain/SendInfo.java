package com.sf.su.marathon.dao.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

@ApiModel("寄件信息")
public class SendInfo {
    private Integer id;


    private String groupId;

    @ApiModelProperty("用户ID")
    private String userId;

    @ApiModelProperty("寄件人姓名")
    private String senderName;

    @ApiModelProperty("寄件人手机号")
    private String senderPhone;

    @ApiModelProperty("预估每日寄件量")
    private Integer dailySendCount;

    @ApiModelProperty("每日平均寄件重量")
    private BigDecimal averageSendWeight;

    @ApiModelProperty("寄件详细地址")
    private String address;

    @ApiModelProperty("省")
    private String province;

    @ApiModelProperty("市")
    private String city;

    @ApiModelProperty("区")
    private String area;



    public SendInfo(Integer id, String userId, String senderName, String senderPhone, Integer dailySendCount, BigDecimal averageSendWeight, String address, String province, String city, String area, String groupId) {
        this.id = id;
        this.userId = userId;
        this.senderName = senderName;
        this.senderPhone = senderPhone;
        this.dailySendCount = dailySendCount;
        this.averageSendWeight = averageSendWeight;
        this.address = address;
        this.province = province;
        this.city = city;
        this.area = area;
        this.groupId = groupId;
    }

    public SendInfo() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName == null ? null : senderName.trim();
    }

    public String getSenderPhone() {
        return senderPhone;
    }

    public void setSenderPhone(String senderPhone) {
        this.senderPhone = senderPhone == null ? null : senderPhone.trim();
    }

    public Integer getDailySendCount() {
        return dailySendCount;
    }

    public void setDailySendCount(Integer dailySendCount) {
        this.dailySendCount = dailySendCount;
    }

    public BigDecimal getAverageSendWeight() {
        return averageSendWeight;
    }

    public void setAverageSendWeight(BigDecimal averageSendWeight) {
        this.averageSendWeight = averageSendWeight;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area == null ? null : area.trim();
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId == null ? null : groupId.trim();
    }

    @Override
    public String toString() {
        return "SendInfo{" +
                "id=" + id +
                ", userId='" + userId + '\'' +
                ", senderName='" + senderName + '\'' +
                ", senderPhone='" + senderPhone + '\'' +
                ", dailySendCount=" + dailySendCount +
                ", averageSendWeight=" + averageSendWeight +
                ", address='" + address + '\'' +
                ", province='" + province + '\'' +
                ", city='" + city + '\'' +
                ", area='" + area + '\'' +
                ", groupId='" + groupId + '\'' +
                '}';
    }
}