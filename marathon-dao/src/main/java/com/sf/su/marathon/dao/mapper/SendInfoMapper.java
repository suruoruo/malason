package com.sf.su.marathon.dao.mapper;

import com.sf.su.marathon.dao.domain.SendInfo;

public interface SendInfoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SendInfo record);

    int insertSelective(SendInfo record);

    SendInfo selectByPrimaryKey(Integer id);

    SendInfo getSendInfoByUserIdAndGroupId(String userId, String groupId);

    int updateByPrimaryKeySelective(SendInfo record);

    int updateByPrimaryKey(SendInfo record);
}