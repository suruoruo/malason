package com.sf.su.marathon.dao.dto;

import java.util.Date;

public class FakeUser {
    private String id;

    private String nickName;

    private String avatarUrl;

    private String openId;

    private Date createdTime;

    private Date modifiedTime;

    public FakeUser(String id, String name, String headImg, String openId, Date createdTime, Date modifiedTime) {
        this.id = id;
        this.nickName = name;
        this.avatarUrl = headImg;
        this.openId = openId;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }

    public FakeUser() {
        super();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }
}
