package com.sf.su.marathon.dao.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by shawn on 2017/12/15.
 */
@ApiModel("集货团详细信息")
public class GroupDetailInfo implements Serializable {


    @ApiModelProperty("集团ID")
    private String groupId;

    @ApiModelProperty("集货团名称")
    private String groupName;


    @ApiModelProperty("到期时间")
    private Date expireDate;

    @ApiModelProperty("当前多少人已报名")
    private Integer currentCount;

    @ApiModelProperty("总共多少人已报名")
    private Integer totalCount;

    @ApiModelProperty("是否满员")
    private Boolean isFull;

    @ApiModelProperty("类型：1.全国集货  2.地区性集货")
    private Integer type;

    @ApiModelProperty("集货团状态 1.集货中  2.时间已截止  3.集货已满")
    private Integer state;

    @ApiModelProperty("创建时间")
    private Date createdTime;


    @ApiModelProperty("单客日均最小件量")
    private Short dailyMinPackages;

    @ApiModelProperty("重量区间（最小）")
    private BigDecimal weightMin;

    @ApiModelProperty("重量区间（最大）")
    private BigDecimal weightMax;

    @ApiModelProperty("首重价格")
    private BigDecimal basePrice;

    @ApiModelProperty("首重重量")
    private BigDecimal baseWeight;


    @ApiModelProperty("承诺周期")
    private Integer promiseDuration;

    @ApiModelProperty("专业要求")
    private String useRequire;


    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public Integer getCurrentCount() {
        return currentCount;
    }

    public void setCurrentCount(Integer currentCount) {
        this.currentCount = currentCount;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Boolean getFull() {
        return isFull;
    }

    public void setFull(Boolean full) {
        isFull = full;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Short getDailyMinPackages() {
        return dailyMinPackages;
    }

    public void setDailyMinPackages(Short dailyMinPackages) {
        this.dailyMinPackages = dailyMinPackages;
    }

    public BigDecimal getWeightMin() {
        return weightMin;
    }

    public void setWeightMin(BigDecimal weightMin) {
        this.weightMin = weightMin;
    }

    public BigDecimal getWeightMax() {
        return weightMax;
    }

    public void setWeightMax(BigDecimal weightMax) {
        this.weightMax = weightMax;
    }

    public BigDecimal getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(BigDecimal basePrice) {
        this.basePrice = basePrice;
    }

    public BigDecimal getBaseWeight() {
        return baseWeight;
    }

    public void setBaseWeight(BigDecimal baseWeight) {
        this.baseWeight = baseWeight;
    }

    public Integer getPromiseDuration() {
        return promiseDuration;
    }

    public void setPromiseDuration(Integer promiseDuration) {
        this.promiseDuration = promiseDuration;
    }

    public String getUseRequire() {
        return useRequire;
    }

    public void setUseRequire(String useRequire) {
        this.useRequire = useRequire;
    }
}
