package com.sf.su.marathon.dao.domain;

import java.util.Date;

public class User {
    private String id;

    private String name;

    private String headImg;

    private String openId;

    private Date createdTime;

    private Date modifiedTime;

    public User(String id, String name, String headImg, String openId, Date createdTime, Date modifiedTime) {
        this.id = id;
        this.name = name;
        this.headImg = headImg;
        this.openId = openId;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
    }

    public User() {
        super();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg == null ? null : headImg.trim();
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId == null ? null : openId.trim();
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }
}