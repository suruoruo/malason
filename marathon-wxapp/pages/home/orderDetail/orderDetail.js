var sendRequest = require('../../../utils/sendRequest.js');
var constants = require('../../../utils/constants.js');
var baseImageUrl = constants.UrlConstants.baseImageUrl;
var stringUtils = require('../../../utils/stringUtils.js')
var utils = require('../../../utils/util')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    show: false,
    orderDetail:{
      groupName:'鞋服专送080901期',
      weightMin:1.5,
      weightMax:5,
      dailyMinPackages:20,
      basePrice: 6,
      baseWeight:1.5,
      expireDate:'8月14日'
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    var that = this;
    this.groupId = options.groupId
    // this.signUP();
    that.getGroupInfo()
    wx.setStorage({
      key: 'success',
      data: '',
    })
    this.getGroupInfo()
  },
  onShow: function () {
    var show = wx.getStorageSync('success')

    this.setData({
      show: show ? true : false
    })
  },


  getGroupInfo: function () {
    var that = this;
    var resourceId = wx.getStorageSync("resourceId")
    sendRequest.send(
      constants.InterfaceUrl.GET_GROUP_DETAIL_INFO,
      { resourceId: resourceId, groupId: that.groupId },
      function (res) {
        console.log(res.data.data)
        if(res.data.data) {
          that.setData({
            orderDetail: res.data.data
          })
        }
      },
      function (err) {

      }
    )
  },

  signUP: function () {
    var that = this;
    var resourceId = wx.getStorageSync("resourceId")
    sendRequest.send(
      constants.InterfaceUrl.GROUP_SIGN_UP,
      { resourceId: resourceId, groupId: "00a2de21ae0b48eaad6a95f62385c3f1" },
      function (res) {
        console.log(res.data.data)
        that.setData({
          orderList: res.data.data
        })
      },
      function (err) {

      }
    )
  },
  share: function () {
    console.log('share')
    wx.redirectTo({
      url: '/pages/home/shareQrcode/shareQrcode',
    })
  },
  closeDialog() {
    this.setData({show:false})
    wx.setStorageSync('success', '')
  }
})