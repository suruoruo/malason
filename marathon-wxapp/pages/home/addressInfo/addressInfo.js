// pages/home/addressInfo/addressInfo.js
var stringUtils = require('../../../utils/stringUtils.js')
var constants = require('../../../utils/constants.js')
var sendRequest = require('../../../utils/sendRequest.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    region:['省','市','区'],
    province: '',
    city: '',
    area: '',
    address: '',
    name: '',
    phone: '',
    orderNum: '',
    aver_weight:'',
    errMsg:'',
    sendInfo:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.groupId = options.groupId
    var that = this
    sendRequest.send(
      constants.InterfaceUrl.QUERY_SEND_INFO,
      {
        userId: wx.getStorageSync('userId'),
        groupId: this.groupId
      },
      function(res) {
        if(res.data.data) {
          that.sendInfo = that.data.data
        }
      },
      function(err) {

      }
    )
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  bindRegionChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    var addr = e.detail.value
    this.setData({
      region: addr,
      province: addr[0],
      city: addr[1],
      area: addr[2]
    })
  },

  //处理地址输入框输入
  handleAddrInput: function(e) {
    console.log('handleAddrInput', e)
    this.setData({
      address: e.detail.value
    })
  },

  //处理姓名输入框输入
  handleNameInput: function(e) {
    this.setData({
      name: e.detail.value
    })
  },

  //处理电话号码输入框输入
  handlePhoneInput: function(e) {
    this.setData({
      phone: e.detail.value
    })
  },

  //处理寄件数量输入框输入
  handleNumInput: function(e) {
    this.setData({
      orderNum: e.detail.value
    })
  },

  //处理单件平均重量输入框输入
  handleWeightInput: function(e) {
    this.setData({
      aver_weight: e.detail.value
    })
  },

  //报名集货
  submit:function() {
    var that = this
    if (!this.validateAddr()) {
      this.setData({
        errMsg: '请完善地址信息！'
      })
      return;
    } else if(!stringUtils.isNotEmpty(this.data.name)) {
      this.setData({
        errMsg: '请完善寄件人信息！'
      })
      return;
    } else if (!stringUtils.isPhoneNumber(this.data.phone)) {
      this.setData({
        errMsg: '电话号码错误！'
      })
      return;
    } else if (!stringUtils.isNotEmpty(this.data.orderNum) || parseInt(this.data.orderNum) < 20) {
      this.setData({
        errMsg: '请正确填写寄件数量！'
      })
      return;
    } else if (!stringUtils.isNotEmpty(this.data.aver_weight) || parseFloat(this.data.aver_weight) <= 0.0) {
      this.setData({
        errMsg: '请正确填写平均重量！'
      })
      return;
    } else {
      this.setData({
        errMsg: ''
      })
      // sendRequest.send(
      //   constants.InterfaceUrl.ADD_SEND_INFO,
      //   {
      //     userId: wx.getStorageSync('userInfo'),
      //     groupId: "0041a7d20b8044d0a9ac439fbc997206",
      //     senderName: that.data.name,
      //     senderPhone: that.data.phone,
      //     averageSendWeight: that.data.aver_weight,
      //     province: that.data.province,
      //     city: that.data.city,
      //     area: that.data.area,
      //     address: that.data.address
      //   },
      //   function(res) {

      //   },
      //   function(err) {

      //   })
      wx.setStorageSync('success', true)
      wx.navigateBack()
    }
  },

  //校验地址是否合法
  validateAddr: function() {
    if(stringUtils.isNotEmpty(this.data.province) && stringUtils.isNotEmpty(this.data.city) && stringUtils.isNotEmpty(this.data.area) && stringUtils.isNotEmpty(this.data.address)) {
      return true
    }
    return false
  }
})