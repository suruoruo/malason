// pages/home/home.js
var sendRequest = require('../../utils/sendRequest.js')
var constants = require('../../utils/constants.js')
var utils = require('../../utils/util')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    utils.login(
      function (res) {
        var resourceId = wx.getStorageSync(constants.StorageConstants.resourceId);
        sendRequest.send(
          constants.InterfaceUrl.GET_GROUP_LIST + '?resourceId=' + resourceId,
          {},
          function (res) {
            console.log(res.data.data)
            that.setData({
              orderList: res.data.data
            })
          },
          function (err) {

          },
          'GET'
        )
      },
      function (err) {

      }
    );
  },
})