/**
 * 网络请求基类
 * @author 01368384
 */
var stringUtils = require('../utils/stringUtils.js');
var constants = require('../utils/constants.js');
var util = require('../utils/util.js')
var baseUrl = constants.UrlConstants.baseUrl;
console.log(baseUrl)
var ERR_CODE = constants.errorCode
function send(url, data, sucFunc, errFunc, method, contentType) {
  var token = wx.getStorageSync(constants.StorageConstants.tokenKey);
  console.log("token: ", token);
  wx.request({
    url: baseUrl + url,
    method: method ? method : 'POST',
    data: data,
    header: {
      "loginToken": token,
      "content-type": contentType ? contentType : "application/x-www-form-urlencoded",
    },
    success: function (res) {
      if (res.statusCode == 200) {
        var errorCode = res.data.errorCode
        if (errorCode == ERR_CODE.SUCCESS || errorCode == ERR_CODE.NEED_SHOW_DIALOG || errorCode == ERR_CODE.NOT_SHOW_DIALOG) {//成功
          sucFunc(res);
        } else if (errorCode == ERR_CODE.RE_LOGIN) {//token失效,重新登录
          wx.showLoading({
            title: '登录中',
            mask: true
          })
          util.login(function () {
            wx.hideLoading()
            send(url, data, sucFunc, errFunc, method, contentType)
          }, function () {
            wx.hideLoading()
            wx.showToast({
              title: '登录失败',
            })
          })
        } else if (errorCode == ERR_CODE.BIND_PHONE) {//未绑定手机
          console.log(url)
          console.log(errorCode)
          errFunc('需绑定手机号')
          wx.redirectTo({
            url: '/pages/user/bindPhone/bindPhone',
          })
         
        } else {
          errFunc(res.data.message);
        }
      } else {
        errFunc('请求错误，请重试');
      }
    },
    fail: function (err) {
      console.log(err);
      errFunc('请求失败，请重试');
    }
  })
}

module.exports = {
  send: send
}
