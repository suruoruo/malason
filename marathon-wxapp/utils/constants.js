/**
 * 配置字段文件类
 * @author 01368384，854638
 */

var release = false;//环境切换开关 true:正式环境  false：测试环境

var baseUrl_sit = "http://10.2.4.87:8080";   //测试环境
var baseUrl_pro = ""; //生产环境
var baseImageUrl_sit = "" //图片前缀测试环境
var baseImageUrl_pro = ""//图片前缀生产环境

/**
 * BaseUrl地址
 */
var UrlConstants = {
  baseUrl: release ? baseUrl_pro : baseUrl_sit,
  baseImageUrl: release ? baseImageUrl_pro : baseImageUrl_sit
}

/**
 * 缓存key
 */
var StorageConstants = {
  userInfoKey: "userInfo",
  loginInfoKey: "loginfo",
  tokenKey: "token",
  resourceId:'resourceId',
  userId: 'userId'
}

/**
 * 业务接口地址
 */
var InterfaceUrl = {
  //---------首页相关接口----------//
  GET_GROUP_LIST : '/group/getGroupList',
  GET_GROUP_DETAIL_INFO : '/group/getGroupDetailInfo',
  //---------寄件相关接口---------//
  ADD_SEND_INFO: '/userInfo/addSendInfo',
  QUERY_SEND_INFO: '/userInfo/querySendInfo',
  GROUP_SIGN_UP:'/group/signup'

  //---------个人中心相关接口----------//
  
}

/**
 * 接口错误Code
 */
var errorCode = {
}

module.exports = {
  UrlConstants: UrlConstants,
  StorageConstants: StorageConstants,
  InterfaceUrl: InterfaceUrl,
  errorCode: errorCode
}