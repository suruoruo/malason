package com.sf.su.marathon.utils;


/**
 * @author wujiang
 * @version 1.0.0
 * @date 2017/12/15
 */
public class Result {

    public static Result buildResp(int status, String msg, Object data) {
        Result resp = new Result();
        resp.setStatus(status);
        resp.setMsg(msg);
        resp.setData(data);
        return resp;
    }

    private int status;
    private String msg;
    private Object data;


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}

