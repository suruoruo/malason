package com.sf.su.marathon.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 *
 * @描述：时间辅助类
 * @日期：2015年2月6日 下午6:33:21
 * @开发人员： MR.X
 */
public final class DateUtils {
	protected final static Logger logger = LoggerFactory.getLogger(DateUtils.class);

    private static Date getBeforeTime(int count,int timeUnit) {
        Calendar cl = Calendar.getInstance();
        cl.add(timeUnit,0-count);
        return cl.getTime();
    }

    /**
     * 得到n分钟前时间
     *
     * @param minute 分钟数
     * @return Date
     */
    public static Date getTimeBeforeMinutes(int minute) {
        return getBeforeTime(minute,Calendar.MINUTE);
    }

}