package com.sf.su.marathon.Const;

/**
 * @author wujiang
 * @version 1.0.0
 * @date 2017/12/15
 */
public class Const {

    private Const(){

    }
    public static final String PUBLICKEY ="js1*12jkljxa1";

    public static final  int SUCCESSSTATUS = 01;
    public static final int FAILSTATUS = 02;
    public static final int NOTEXIST = 03;
}
