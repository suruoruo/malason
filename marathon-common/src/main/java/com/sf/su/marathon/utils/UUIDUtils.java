package com.sf.su.marathon.utils;

import java.util.UUID;

/**
 * @author 591030 on 2017/12/15.
 */
public class UUIDUtils {
    public static String getUUID(){
        return UUID.randomUUID().toString().replace("-", "");
    }
}
