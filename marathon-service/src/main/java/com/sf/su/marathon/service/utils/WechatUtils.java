//package com.sf.su.marathon.service.utils;
//
//import com.sf.su.marathon.Const.Const;
//import com.sf.su.marathon.model.OpenidResult;
//import com.sf.su.marathon.utils.AesUtils;
//import com.sf.su.marathon.utils.HttpClientUtils;
//import org.apache.commons.lang3.StringUtils;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Service;
//import com.alibaba.fastjson.JSON;
//
///**
// * @author wujiang
// * @version 1.0.0
// * @date 2017/12/15
// */
//@Service
//public class WechatUtils {
//    private static Logger log = LoggerFactory.getLogger(WechatUtils.class);
//
//    @Value("${appId}")
//    private String appId;
//    @Value("${appSecret}")
//    private String appSecret;
//    @Value("${openid.url}")
//    private String openIdUrl;
//
//    public String getResourceId(String jsCode) {
//        String url = openIdUrl + "?appid=" + appId + "&secret=" + appSecret
//                + "&js_code=" + jsCode + "&grant_type=authorization_code";
//        OpenidResult userinfoReturn = JSON.parseObject(HttpClientUtils.sendGet(url), OpenidResult.class);
//        if (userinfoReturn != null) {
//            if (!StringUtils.isEmpty(userinfoReturn.getErrcode())) {
//                return userinfoReturn.getErrmsg();
//            }
//            String openid = userinfoReturn.getOpenid();
//            String resourceId = AesUtils.AESEncode(Const.PUBLIC_KEY, openid);
//            return resourceId;
//        } else {
//            return null;
//        }
//    }
//}
