package com.sf.su.marathon.service;

import com.sf.su.marathon.dao.domain.JoinGroupList;

public interface JoinGroupListService {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(JoinGroupList record);

    JoinGroupList selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(JoinGroupList record);

    int updateByPrimaryKey(JoinGroupList record);

    int countUserByGroup(String groupId);
}
