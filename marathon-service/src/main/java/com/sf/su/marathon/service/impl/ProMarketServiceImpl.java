package com.sf.su.marathon.service.impl;

import com.sf.su.marathon.dao.domain.ProMarket;
import com.sf.su.marathon.manager.ProMarketManager;
import com.sf.su.marathon.service.ProMarketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by shawn on 2017/12/15.
 */
@Service
public class ProMarketServiceImpl implements ProMarketService {

    @Autowired
    private ProMarketManager proMarketManager;

    @Override
    public ProMarket getProMarketInfo(String marketId) {
        return proMarketManager.getProMarketInfo(marketId);
    }

    @Override
    public int deleteByPrimaryKey(String mktId) {
        return proMarketManager.deleteByPrimaryKey(mktId);
    }

    @Override
    public int insertSelective(ProMarket record) {
        return proMarketManager.insertSelective(record);
    }

    @Override
    public ProMarket selectByPrimaryKey(String mktId) {
        return proMarketManager.selectByPrimaryKey(mktId);
    }

    @Override
    public int updateByPrimaryKeySelective(ProMarket record) {
        return proMarketManager.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKeyWithBLOBs(ProMarket record) {
        return updateByPrimaryKeyWithBLOBs(record);

    }

    @Override
    public List<ProMarket> getMarketList() {
        return proMarketManager.getMarketList();
    }
}
