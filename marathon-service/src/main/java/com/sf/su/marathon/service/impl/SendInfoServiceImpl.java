package com.sf.su.marathon.service.impl;

import com.sf.su.marathon.dao.domain.SendInfo;
import com.sf.su.marathon.manager.SendInfoManager;
import com.sf.su.marathon.service.SendInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by 854638 on 2017/12/15.
 */
@Service
public class SendInfoServiceImpl implements SendInfoService {

    @Autowired
    private SendInfoManager sendInfoManager;

    @Override
    public int insert(SendInfo sendInfo) {
        return sendInfoManager.insert(sendInfo);
    }

    @Override
    public int updateSendInfo(SendInfo sendInfo) {
        return sendInfoManager.updateSendInfo(sendInfo);
    }

    @Override
    public SendInfo getSendInfoByUserIdAndGroupId(String userId, String groupId) {
        return sendInfoManager.getSendInfoByUserIdAndGroupId(userId, groupId);
    }
}
