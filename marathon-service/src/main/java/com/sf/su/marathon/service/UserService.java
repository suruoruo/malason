package com.sf.su.marathon.service;

import com.sf.su.marathon.dao.domain.User;

import java.util.List;

/**
 * @author 591030 on 2017/12/14.
 */
public interface UserService {
    int insert(User user);

    User getById(String id);

    User getUserByResourceId(String resourceId);

    /**
     * 根据 集货团ID 获取 所有的用户信息
     * @param groupId
     * @return
     */
    //List<User> getUserInGroup(String groupId);
}
