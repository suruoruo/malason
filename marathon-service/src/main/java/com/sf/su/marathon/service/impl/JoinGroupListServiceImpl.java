package com.sf.su.marathon.service.impl;

import com.sf.su.marathon.dao.domain.JoinGroupList;
import com.sf.su.marathon.manager.JoinGroupListManager;
import com.sf.su.marathon.service.JoinGroupListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JoinGroupListServiceImpl implements JoinGroupListService{
    @Autowired
    private JoinGroupListManager joinGroupListManager;


    @Override
    public int deleteByPrimaryKey(Integer id) {
        return joinGroupListManager.deleteByPrimaryKey(id);
    }

    @Override
    public int insertSelective(JoinGroupList record) {
        return joinGroupListManager.insertSelective(record);
    }

    @Override
    public JoinGroupList selectByPrimaryKey(Integer id) {
        return joinGroupListManager.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(JoinGroupList record) {
        return joinGroupListManager.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(JoinGroupList record) {
        return joinGroupListManager.updateByPrimaryKeySelective(record);
    }

    @Override
    public int countUserByGroup(String groupId) {
        return joinGroupListManager.countUserByGroup(groupId);
    }
}
