package com.sf.su.marathon.service;

import com.sf.su.marathon.dao.domain.SendInfo;

/**
 * Created by 854638 on 2017/12/15.
 */
public interface SendInfoService {
    int insert(SendInfo sendInfo);

    int updateSendInfo(SendInfo sendInfo);

    SendInfo getSendInfoByUserIdAndGroupId(String userId, String groupId);
}
