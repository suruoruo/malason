package com.sf.su.marathon.service.impl;

import com.sf.su.marathon.Const.Const;
import com.sf.su.marathon.dao.domain.User;
import com.sf.su.marathon.manager.UserManager;
import com.sf.su.marathon.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author 591030 on 2017/12/14.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserManager userManager;
    @Override
    public int insert(User user) {
        return userManager.insert(user);
    }

    @Override
    public User getById(String id) {
        return userManager.getById(id);
    }

    @Override
    public User getUserByResourceId(String resourceId) {
        String openId= resourceId;
        return userManager.getByOpenId(openId);
    }

}
