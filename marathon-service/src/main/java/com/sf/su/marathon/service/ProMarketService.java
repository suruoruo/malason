package com.sf.su.marathon.service;

import com.sf.su.marathon.dao.domain.ProMarket;

import java.util.List;

/**
 * Created by shawn on 2017/12/15.
 */
public interface ProMarketService {

    /**
     * 获取 专业市场ID
     * @param marketId
     * @return
     */
    ProMarket getProMarketInfo(String marketId);

   int deleteByPrimaryKey(String mktId);

    int insertSelective(ProMarket record);

    ProMarket selectByPrimaryKey(String mktId);

    int updateByPrimaryKeySelective(ProMarket record);

    int updateByPrimaryKeyWithBLOBs(ProMarket record);

    List<ProMarket> getMarketList();

}
