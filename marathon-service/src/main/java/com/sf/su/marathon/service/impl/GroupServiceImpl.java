package com.sf.su.marathon.service.impl;

import com.sf.su.marathon.dao.domain.Group;
import com.sf.su.marathon.dao.domain.User;
import com.sf.su.marathon.dao.dto.GroupDetailInfo;
import com.sf.su.marathon.manager.GroupManager;
import com.sf.su.marathon.service.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**

 * Created by shawn on 2017/12/15.
 */

@Service
public class GroupServiceImpl implements GroupService {

  @Autowired
    private GroupManager groupManager;

    @Override

    public Group selectGroupById(String groupId) {
        return groupManager.selectGroupById(groupId);
    }

    public int insert(Group group) {
        return groupManager.insert(group);
    }

    @Override
    public List<User> getUserInGroup(String groupId) {
        return groupManager.getUserInGroup(groupId);
    }

    @Override
    public List<GroupDetailInfo> getGroupList() {
        return groupManager.getGroupList();
    }

    @Override
    public boolean signup(String openId, Group group) {
        return groupManager.signup(openId,group);
    }
}
