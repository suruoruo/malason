package com.sf.su.marathon.service;

import com.sf.su.marathon.dao.domain.Group;
import com.sf.su.marathon.dao.domain.User;
import com.sf.su.marathon.dao.dto.GroupDetailInfo;

import java.util.List;

/**
 * Created by shawn on 2017/12/15.
 */
public interface GroupService {

    /**
     * 通过ID获取集货团信息
     */
    Group selectGroupById(String groupId);


    int insert(Group group);

    boolean signup(String openId,Group group);

    /**
     * 获取 集货团中所有报名的用户
     * @param groupId
     * @return
     */
    List<User> getUserInGroup(String groupId);

    /**
     * 获取 集货团列表
     * @return
     */
    List<GroupDetailInfo> getGroupList();
}
