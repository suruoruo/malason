package com.sf.su.marathon.web.controller;

import com.sf.su.marathon.Const.Const;
import com.sf.su.marathon.dao.domain.ProMarket;
import com.sf.su.marathon.service.GroupService;
import com.sf.su.marathon.service.ProMarketService;
import com.sf.su.marathon.service.UserService;
import com.sf.su.marathon.utils.Result;
import com.sf.su.marathon.utils.UUIDUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shawn on 2017/12/15.
 */
@RequestMapping("group")
@Controller
@Api("专业市场")
public class ProMarketController {


    private static final Logger LOGGER = LoggerFactory.getLogger(GroupController.class);


    @Autowired
    private GroupService groupService;

    @Autowired
    private ProMarketService proMarketService;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "getMarketList",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation("根据集团ID 获取用户详细信息")
    public Result getMarketList() {

        List<ProMarket> proMarketList = new ArrayList<>();

        try{
            proMarketList = proMarketService.getMarketList();

            return Result.buildResp(Const.SUCCESSSTATUS,"succeed to get promarket list",proMarketList);
        } catch (Exception e){
            LOGGER.warn("查询出错",e.getMessage());
            return Result.buildResp(Const.FAILSTATUS,"fail get promarket list",null);
        }

    }

    @RequestMapping(value = "insertMarket",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation("添加专业市场")
    public Result insert(ProMarket proMarket) {
        try{
            proMarket.setMktId(UUIDUtils.getUUID());
            int result = proMarketService.insertSelective(proMarket);
            return Result.buildResp(Const.SUCCESSSTATUS,"succeed to add proMark",result);
        } catch (Exception e){
           LOGGER.warn(e.getMessage());
        }
        return Result.buildResp(Const.FAILSTATUS,"fail add proMark ",null);

    }

}
