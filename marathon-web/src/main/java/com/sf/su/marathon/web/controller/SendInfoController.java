package com.sf.su.marathon.web.controller;

import com.alibaba.fastjson.JSON;
import com.sf.su.marathon.Const.Const;
import com.sf.su.marathon.dao.domain.SendInfo;
import com.sf.su.marathon.service.SendInfoService;
import com.sf.su.marathon.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by 854638 on 2017/12/15.
 */
@RequestMapping("userInfo")
@Controller
public class SendInfoController {

    @Autowired
    private SendInfoService sendInfoService;

    @RequestMapping(value = "addSendInfo",method = RequestMethod.POST)
    @ResponseBody
    public Result addSendInfo(String sendInfo) {
        SendInfo info = JSON.parseObject(sendInfo, SendInfo.class);
        String userId = info.getUserId();
        String groupId = info.getGroupId();
        SendInfo si = sendInfoService.getSendInfoByUserIdAndGroupId(userId, groupId);
        if(si != null) {       //不为空，更新
            int res = sendInfoService.updateSendInfo(info);
            if(res > 0) {
                return Result.buildResp(Const.SUCCESSSTATUS,"succeed to update sendInfo", true);
            } else {
               return Result.buildResp(Const.FAILSTATUS,"failed to update sendInfo", false);
            }
        } else {      //为空，新增插入
            int res = sendInfoService.insert(info);
            if(res > 0) {
                return  Result.buildResp(Const.SUCCESSSTATUS,"succeed to insert sendInfo", true);
            } else {
                return Result.buildResp(Const.FAILSTATUS,"failed to insert sendInfo", false);
            }
        }
    }

    @RequestMapping(value = "querySendInfo", method = RequestMethod.POST)
    @ResponseBody
    public Result querySendInfo(String userId, String groupId) {
        try {
            SendInfo sendInfo = sendInfoService.getSendInfoByUserIdAndGroupId(userId, groupId);
            return Result.buildResp(Const.SUCCESSSTATUS, "succeed to query sendInfo", sendInfo);
        } catch (Exception e) {
            return Result.buildResp(Const.FAILSTATUS, e.getMessage(), false);
        }
    }

    public static void main(String[] args) {

    }
}
