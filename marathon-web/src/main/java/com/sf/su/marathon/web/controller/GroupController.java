package com.sf.su.marathon.web.controller;

import com.sf.su.marathon.Const.Const;
import com.sf.su.marathon.dao.domain.Group;
import com.sf.su.marathon.dao.domain.ProMarket;
import com.sf.su.marathon.dao.domain.User;
import com.sf.su.marathon.dao.dto.GroupDetailInfo;
import com.sf.su.marathon.service.GroupService;
import com.sf.su.marathon.service.JoinGroupListService;
import com.sf.su.marathon.service.ProMarketService;
import com.sf.su.marathon.service.UserService;
import com.sf.su.marathon.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shawn on 2017/12/15.
 */
@RequestMapping("group")
@Controller
@Api("集货团")
public class GroupController {

    private static final Logger LOGGER = LoggerFactory.getLogger(GroupController.class);


    @Autowired
    private GroupService groupService;

    @Autowired
    private ProMarketService proMarketService;

    @Autowired
    private UserService userService;

    @Autowired
    private JoinGroupListService joinGroupListService;

    @RequestMapping(value = "getGroupDetailInfo", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation("根据集团ID 获取用户详细信息")
    public Result getGroupDetailInfo(String resourceId, String groupId) {

        GroupDetailInfo groupDetailInfo = new GroupDetailInfo();

        try {

            User user = userService.getUserByResourceId(resourceId);

            if (null == user) {
                return Result.buildResp(Const.NOTEXIST, "user is not exist", null);

            }

            Group group = groupService.selectGroupById(groupId);

            ProMarket proMarket = proMarketService.getProMarketInfo(group.getProMarketId());

            groupDetailInfo.setGroupId(groupId);
            BeanUtils.copyProperties(group, groupDetailInfo);
            BeanUtils.copyProperties(proMarket, groupDetailInfo);


            return Result.buildResp(Const.SUCCESSSTATUS, "succeed to get resourceId", groupDetailInfo);
        } catch (Exception e) {
            LOGGER.warn("查询出错", e.getMessage());

            return Result.buildResp(Const.FAILSTATUS, "requesting wechat can not get value", null);
        }
    }


    @RequestMapping(value = "getUserInGroup", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation("根据集团ID 获取参加的用户信息")
    public Result getUserInGroup(String resourceId, String groupId) {

        List<User> userList = new ArrayList<>();
        try {

            User user = userService.getUserByResourceId(resourceId);

            if (null == user) {
                return Result.buildResp(Const.NOTEXIST, "user is not exist", null);

            }

            userList = groupService.getUserInGroup(groupId);

            return Result.buildResp(Const.SUCCESSSTATUS,"succeed to get resourceId",userList);
        } catch (Exception e){
            LOGGER.warn("查询出错",e.getMessage());
            return Result.buildResp(Const.FAILSTATUS,"requesting wechat can not get value",null);
        }

    }




    @RequestMapping(value = "getGroupList",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation("获取列表信息")
    public Result getGroupList(String resourceId) {

        List<GroupDetailInfo> groupList = new ArrayList<>();
        try{

            User user = userService.getUserByResourceId(resourceId);

            if(null == user){
                return Result.buildResp(Const.NOTEXIST,"user is not exist",null);

            }

            groupList = groupService.getGroupList();

            return Result.buildResp(Const.SUCCESSSTATUS,"succeed to get resourceId",groupList);
        } catch (Exception e){
            LOGGER.warn("查询出错",e.getMessage());
            return Result.buildResp(Const.SUCCESSSTATUS, "succeed to get resourceId", null);
        }

    }

    @RequestMapping(value = "signup", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation("报名")
    public Result signup(String resourceId, String groupId) {
        Group group = groupService.selectGroupById(groupId);
        User user = userService.getUserByResourceId(resourceId);
        if(user == null){
            return Result.buildResp(Const.FAILSTATUS, "未登陆", false);

        }
        String openId=user.getOpenId();

        try {
            int currentCount = joinGroupListService.countUserByGroup(groupId);
            currentCount++;
            if (currentCount > group.getTotalCount()) {
                group.setIsFull(true);
                return Result.buildResp(Const.FAILSTATUS, "报名人数已满", null);
            }
            group.setCurrentCount(currentCount);
            if (groupService.signup(openId,group)){
                return Result.buildResp(Const.SUCCESSSTATUS, "报名成功", true);

            }else {
                return Result.buildResp(Const.FAILSTATUS, "报名失败", false);

            }
        } catch (Exception e) {
            LOGGER.warn("查询出错", e.getMessage());
            return Result.buildResp(Const.FAILSTATUS, "requesting wechat can not get value", null);
        }
    }

}
