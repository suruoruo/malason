package com.sf.su.marathon.manager;

import com.sf.su.marathon.dao.domain.ProMarket;

import java.util.List;

import java.util.List;

public interface ProMarketManager {
    ProMarket getProMarketInfo(String marketId);

    int deleteByPrimaryKey(String mktId);

    int insertSelective(ProMarket record);

    ProMarket selectByPrimaryKey(String mktId);

    int updateByPrimaryKeySelective(ProMarket record);

    int updateByPrimaryKeyWithBLOBs(ProMarket record);

    List<ProMarket> getMarketList();

    List<ProMarket> selectAll();

}
