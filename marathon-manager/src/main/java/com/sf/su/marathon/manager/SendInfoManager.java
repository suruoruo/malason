package com.sf.su.marathon.manager;

import com.sf.su.marathon.dao.domain.SendInfo;
import org.springframework.stereotype.Component;

/**
 * Created by 854638 on 2017/12/15.
 */
public interface SendInfoManager {
    int insert(SendInfo sendInfo);

    int updateSendInfo(SendInfo sendInfo);

    SendInfo getSendInfoByUserIdAndGroupId(String userId, String groupId);
}
