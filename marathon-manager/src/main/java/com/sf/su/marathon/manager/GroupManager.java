package com.sf.su.marathon.manager;

import com.sf.su.marathon.dao.domain.Group;
import com.sf.su.marathon.dao.domain.User;
import com.sf.su.marathon.dao.dto.GroupDetailInfo;

import java.util.List;

/**
 * Created by shawn on 2017/12/15.
 */
public interface GroupManager {

    /**
     * 根据集货团ID获取集货团信息
     * @param groupId
     * @return
     */
    Group selectGroupById(String groupId);

    int insert(Group group);

    int updateGroup(Group record);

    boolean signup(String openId,Group group);
    List<Group> selectNotFullAndExpire();

    List<Group> getByMarketId(String mktId);

    /**
     * 根据集货团ID获取 所有参与用户的信息
     * @param groupId
     * @return
     */
    List<User> getUserInGroup(String groupId);

    /**
     * 获取所有集货团 列表
     * @return
     */
    List<GroupDetailInfo> getGroupList();

}
