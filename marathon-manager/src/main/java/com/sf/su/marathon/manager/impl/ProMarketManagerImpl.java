package com.sf.su.marathon.manager.impl;

import com.sf.su.marathon.dao.domain.ProMarket;
import com.sf.su.marathon.dao.mapper.ProMarketMapper;
import com.sf.su.marathon.manager.ProMarketManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProMarketManagerImpl implements ProMarketManager{

    @Autowired
    ProMarketMapper proMarketMapper;

    @Override
    public int deleteByPrimaryKey(String mktId) {
        return proMarketMapper.deleteByPrimaryKey(mktId);
    }

    @Override
    public int insertSelective(ProMarket record) {
        return proMarketMapper.insertSelective(record);
    }

    @Override
    public ProMarket selectByPrimaryKey(String mktId) {
        return proMarketMapper.selectByPrimaryKey(mktId);
    }

    @Override
    public int updateByPrimaryKeySelective(ProMarket record) {
        return proMarketMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKeyWithBLOBs(ProMarket record) {
        return proMarketMapper.updateByPrimaryKeyWithBLOBs(record);
    }

    @Override
    public List<ProMarket> getMarketList() {
        return proMarketMapper.getMarketList();
    }

    @Override
    public List<ProMarket> selectAll() {
        return proMarketMapper.selectAll();
    }

    public ProMarket getProMarketInfo(String marketId) {
        return proMarketMapper.selectByPrimaryKey(marketId);
    }

}
