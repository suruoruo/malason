package com.sf.su.marathon.manager.impl;

import com.sf.su.marathon.dao.domain.User;
import com.sf.su.marathon.dao.mapper.UserMapper;
import com.sf.su.marathon.manager.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author 591030 on 2017/12/14.
 */
@Component
public class UserManagerImpl implements UserManager {

    @Autowired
    private UserMapper userMapper;

    @Override
    public int insert(User user) {
        return userMapper.insertSelective(user);
    }

    @Override
    public User getById(String id) {
        return userMapper.selectByPrimaryKey(id);
    }

    @Override
    public User getByOpenId(String openId) {
        return userMapper.selectByOpenId(openId);
    }


}
