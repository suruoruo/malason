package com.sf.su.marathon.manager;

import com.sf.su.marathon.dao.domain.User;
import org.springframework.stereotype.Component;

/**
 * @author 591030 on 2017/12/14.
 */
public interface UserManager {

    int insert(User user);

    User getById(String id);

    User getByOpenId(String openId);
}
