package com.sf.su.marathon.manager;

import com.sf.su.marathon.dao.domain.JoinGroupList;

public interface JoinGroupListManager {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(JoinGroupList record);

    JoinGroupList selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(JoinGroupList record);

    int updateByPrimaryKey(JoinGroupList record);

    int countUserByGroup(String groupId);
}
