package com.sf.su.marathon.manager.impl;

import com.sf.su.marathon.dao.domain.SendInfo;
import com.sf.su.marathon.dao.mapper.SendInfoMapper;
import com.sf.su.marathon.manager.SendInfoManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by 854638 on 2017/12/15.
 */
@Component
public class SendInfoManagerImpl implements SendInfoManager {

    @Autowired
    private SendInfoMapper sendInfoMapper;

    @Override
    public int insert(SendInfo sendInfo) {
        return sendInfoMapper.insert(sendInfo);
    }

    @Override
    public int updateSendInfo(SendInfo sendInfo) {
        return sendInfoMapper.updateByPrimaryKeySelective(sendInfo);
    }

    @Override
    public SendInfo getSendInfoByUserIdAndGroupId(String userId, String groupId) {
        return sendInfoMapper.getSendInfoByUserIdAndGroupId(userId, groupId);
    }
}
