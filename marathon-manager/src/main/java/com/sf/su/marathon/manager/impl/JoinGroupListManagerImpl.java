package com.sf.su.marathon.manager.impl;

import com.sf.su.marathon.dao.domain.JoinGroupList;
import com.sf.su.marathon.dao.mapper.JoinGroupListMapper;
import com.sf.su.marathon.manager.JoinGroupListManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class JoinGroupListManagerImpl implements JoinGroupListManager{
    @Autowired
    private JoinGroupListMapper joinGroupListMapper;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return joinGroupListMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insertSelective(JoinGroupList record) {
        return joinGroupListMapper.insertSelective(record);
    }

    @Override
    public JoinGroupList selectByPrimaryKey(Integer id) {
        return joinGroupListMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(JoinGroupList record) {
        return joinGroupListMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(JoinGroupList record) {
        return joinGroupListMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int countUserByGroup(String groupId) {
        return joinGroupListMapper.countUserByGroup(groupId);
    }
}
